function varargout = GenGroup(varargin)
% GENGROUP MATLAB code for GenGroup.fig
%      GENGROUP, by itself, creates a new GENGROUP or raises the existing
%      singleton*.
%
%      H = GENGROUP returns the handle to a new GENGROUP or the handle to
%      the existing singleton*.
%
%      GENGROUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GENGROUP.M with the given input arguments.
%
%      GENGROUP('Property','Value',...) creates a new GENGROUP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GenGroup_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GenGroup_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GenGroup

% Last Modified by GUIDE v2.5 25-Sep-2020 10:28:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GenGroup_OpeningFcn, ...
                   'gui_OutputFcn',  @GenGroup_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GenGroup is made visible.
function GenGroup_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GenGroup (see VARARGIN)

% Choose default command line output for GenGroup
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GenGroup wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global filename;
global stuData;
filename='/Users/venice/annCourse/点名册.xlsx';
[num,cell,raw]=xlsread(filename);
stuData=raw
set(handles.stuTbl,'data',raw)


% --- Outputs from this function are returned to the command line.
function varargout = GenGroup_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in readStuBtn.
function readStuBtn_Callback(hObject, eventdata, handles)
% hObject    handle to readStuBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[Fnameh,Pnameh]=uigetfile('*.*');%Fnameh显示的文件名称，Pnameh显示的文件路径
global filename;
global stuData;
filename=[Pnameh,Fnameh]%存储文件的路径及名称
[num,cell,raw]=xlsread(filename);
stuData=raw
set(handles.stuTbl,'data',raw)


% --- Executes on button press in genGroupBtn.
function genGroupBtn_Callback(hObject, eventdata, handles)
% hObject    handle to genGroupBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filename;
global stuData;
global groupData;
% 随机分组
% 交叉验证函数crossvalind('Kfold',N,K)将N个数分为K组
% 结果是各行数据的分组标号
N=length(stuData)
K=floor(N/3)
indices =crossvalind('Kfold', N, K)
groupData=num2cell(zeros(K,5));
for i=1:K
    groupData{i,1}=i;
end
groupData
for i=1:N
    for j=1:5
        if groupData{indices(i),j}==0
            groupData{indices(i),j}=sprintf('%s%s',stuData{i,2},stuData{i,3});
            break;
        end
    end
end
groupData
set(handles.groupTbl,'data',groupData)


% --- Executes on button press in saveGroupBtn.
function saveGroupBtn_Callback(hObject, eventdata, handles)
% hObject    handle to saveGroupBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filename='/Users/venice/annCourse/分组结果.csv';
global groupData;
% 数字转字符串
% groupData=cellfun(@num2str,groupData,'un',0)
% d=cell2mat(groupData)
N=length(groupData)
% for i=1:N
% 	for j=1:5
%         groupData{i,j}
%         csvwrite(filename,groupData{i,j},i,j);
%     end
% end
% csvwrite(filename,num2str(groupData{13,2}),13,2);
fid=fopen(filename,'w');
if fid<0
	errordlg('File creation failed','Error');
end
fprintf(fid,'%s,%s,%s,%s,%s\n','组别','成员1','成员2','成员3','成员4');
for i=1:N
    fprintf(fid,'%d,%s,%s,%s,%s\n',groupData{i,1},groupData{i,2},groupData{i,3},groupData{i,4},groupData{i,5});
end