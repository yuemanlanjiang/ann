% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 单个感知器训练算法实例二：使用神经网络工具箱创建感知器，对应SinglePerceptron.m
% Author：daizhe
% Date:2020-09-16
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear cls;

% 生成一个感知器神经网络并返回给net
net=newp([-20,20;-20,20],1);
% 样本数据
X=[-9,1,-12,-4,0,5;
    15,-8,4,5,11,9];
% X=[-9,-1,-12,-4,0,5;
%     15,0,4,5,11,9];
% 标签
T=[0,1,0,0,0,1];
% 设置最大轮次
net.trainParam.epochs=20;
% 训练网络
net=train(net,X,T);
% 查看训练结果权重值
net.iw{1,1}
% 查看训练结果偏置值
net.b{1}
% 用输入仿真
Y=sim(net,X)