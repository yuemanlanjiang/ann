% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 单个感知器训练算法实例一：计算课件中是算例，激活函数采用单位阶跃函数
% 实现方法2：终止条件是所有样本训练正确，用数组记录样本是否正确的状态
% Author：daizhe
% Date:2020-09-16
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear,clc
% 课件中的算例
% 输入
% X=[1,0;0,1;0.5,1];
% 输出
% Y=[1;0;0];

% 其他例子
X=[-9,15;-1,0;-12,4;-4,5;0,11;5,9]
Y=[0,1,0,0,0,1];


% M:训练数据个数
% N:训练数据维数
[M,N]=size(X);

% 初始化学习速率为1
learnRate=0.1;
% 初始化权值向量
w=zeros(1,N+1);
% 将阈值合并到权值向量
theta=-1.*ones(M,1);
XX=[X,theta];

% 数组的每一位对应一个样本，能够正确训练记为1否则为0
rightNum=zeros(1,M)
% 开始训练
i=1;
n=1;
while sum(rightNum,2)<M
    disp(['第',num2str(n),'次'])
    XX(i,:)
    w
    w*XX(i,:)'
    y=activationFunc(w*XX(i,:)')
    Y(i)
    if Y(i)==y
        rightNum(i)=1;
    else
        rightNum(i)=0;
    end
    rightNum
%     更新权值
    w=w+learnRate.*(Y(i)-y).*XX(i,:)
%     循环使用M个样本数据
    i=mod(i,M)+1
%     迭代次数
    n=n+1
end

% 输出结果
disp('执行结果：')
w

% 图形显示结果
figure;
for i=1:M
    if(Y(i)==0)
        plot(X(i,1),X(i,2),'ro');
    else
        plot(X(i,1),X(i,2),'b*');
    end
    hold on;
end
x=min(X,[],1):0.2:max(X,[],1);
y=(-w(1)*x+w(3))/w(2);
plot(x,y);
hold off;


% 激活函数
% 单位阶跃函数
function [y]=activationFunc(x)
    if x>=0
        y=1;
    else
        y=0;
    end
end