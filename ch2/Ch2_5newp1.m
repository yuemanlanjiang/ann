% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 使用神经网络工具箱创建感知器，训练及仿真
% Author：daizhe
% Date:2020-09-16
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear clc;
% R*2的矩阵，R是输入向量的维数，表示每个输入分量的范围，第二列数字必须大于第一列数字
p=[0 1;-2 2]
% 输出结点个数
t=1;
% 生成一个感知器神经网络并返回给net
net=newp(p,t);
% 样本数据
X=[0 0 1 1;0 1 0 1];
% 标签
T=[0 1 1 1];
% 绘制样本点及其类别
plotpv(X,T)
% 设置迭代次数
net.trainParam.epochs=20;
% 训练网络
net=train(net,X,T);
% 查看训练结果权重值
net.iw{1,1}
% 查看训练结果偏置值
net.b{1}
% 绘制分界线
plotpc(net.iw{1,1},net.b{1})

% 用输入仿真
Y=net(X)
% 也可以用sim函数进行仿真
% 定义一个新的测试数据
testP=[0,0.9]'
testY=sim(net,testP)

plotpv(testP,testY)
hold on;
plotpv(X,Y);
plotpc(net.IW{1},net.b{1});
hold off;
% plot(0,0.9,'*r')