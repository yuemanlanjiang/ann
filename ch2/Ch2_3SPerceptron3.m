% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 单个感知器训练算法实例一：
% 实现方法3：终止条件是对当前轮次中的每个样本都能正确得到输出
% Author：daizhe
% Date:2020-10-05
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear,clc
% 训练样本
X=[-9,15;-1,0;-12,4;-4,5;0,11;5,9]
Y=[0,1,0,0,0,1];


% M:训练数据个数
% N:训练数据维数
[M,N]=size(X);

% 初始化学习速率为1
learnRate=0.1;
% 初始化权值向量
w=zeros(1,N+1);
% 将阈值合并到权值向量
theta=-1.*ones(M,1);
XX=[X,theta];

% 记录当前的轮次
epoch=1;
% 能够每个轮次中能够正确识别的样本数，等于M时停止
rightNum=0;
% 开始训练
i=1;
n=1;
while rightNum<M
    disp(['第',num2str(epoch),'轮'])
    rightNum=0;
    for i=1:M
        disp(['第',num2str(i),'次'])
        XX(i,:)
        w
        w*XX(i,:)'
        y=activationFunc(w*XX(i,:)')
        Y(i)
        if Y(i)==y
            rightNum=rightNum+1
        end
        % 更新权值
        w=w+learnRate.*(Y(i)-y).*XX(i,:)
    end
    epoch=epoch+1;
end

% 输出结果
disp('执行结果：')
w

% 图形显示结果
figure;
for i=1:M
    if(Y(i)==0)
        plot(X(i,1),X(i,2),'ro');
    else
        plot(X(i,1),X(i,2),'b*');
    end
    hold on;
end
x=min(X,[],1):0.2:max(X,[],1);
y=(-w(1)*x+w(3))/w(2);
plot(x,y);
hold off;


% 激活函数
% 单位阶跃函数
function [y]=activationFunc(x)
    if x>=0
        y=1;
    else
        y=0;
    end
end