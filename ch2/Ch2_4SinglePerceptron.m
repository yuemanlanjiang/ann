% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 单个感知器训练算法实例二：全批量训练方法的实现，使用hardlim激活函数
% Author：daizhe
% Date:2020-09-16
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear,clc
% 学习速率
n=0.2;
% 初始化权值
w=[0,0,0];
% 样本数据
% X=[-9,1,-12,-4,0,5;
%     15,-8,4,5,11,9];
X=[-9,-1,-12,-4,0,5;
    15,0,4,5,11,9];
% 期望输出
T=[0,1,0,0,0,1];

% 样本数据合并偏置神经元
X=[X;ones(1,6)];
% 最大轮数
MAX_EPOCH=200;

% 开始训练
i=0;
while 1
    v=w*X;
%     实际输出
    y=hardlim(v);
%     误差
    e=(T-y);
%     误差的平均绝对差
    ee(i+1)=mae(e);
%     小于误差容限，则结束
    if(ee(i+1)<0.001)
        disp('we have got it:');
        disp(w);
        disp('total epoch:');
        disp(i);
        break;
    end
%     更新权值
    w=w+n*(T-y)*X';
    i=i+1;
%     达到最大轮次，结束，训练失败
    if(i>=MAX_EPOCH)
        disp('Max epoch');
        disp(w);
        disp(ee(i+1));
        break;
    end
end
w

% 绘制图形
figure;
% 将样本数据按照类标号标注图中
subplot(2,1,1);
[M,N]=size(X);
for i=1:N
    if(T(i)==0)
        plot(X(1,i),X(2,i),'ro');
    else
        plot(X(1,i),X(2,i),'b*');
    end
    hold on;
end
% 绘制训练结果曲线
x=min(X,[],2):0.2:max(X,[],2);
y=(-w(1)*x-w(3))/w(2);
plot(x,y);
hold off;

% 误差变化曲线
subplot(2,1,2);
x=0:length(ee)-1;
plot(x,ee,'o-');
s=sprintf('mae的值（迭代次数:%d)',i+1);
title(s)
