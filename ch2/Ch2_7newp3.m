% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 神经网络工具箱的应用：奇异样本对训练的影响
% Author：daizhe
% Date:2020-09-16
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
clear,clc

X=[-0.5,-0.5,0.3,-0.1,-30;-0.5,0.5,-0.5,1.0,40];
% X=[-0.5,-0.5,0.3,-0.1;-0.5,0.5,-0.5,1.0];
Y=[1,1,0,0,1];
% 绘制样本点图像
% plotpv(X,Y);
% 创建感知器网络
net=newp([-30,1;-1,40],1);
% pause
% plotpv(X,Y);
% linehandle=plotpv(net.IW{1},net.b{1});
% cla;
% plotpv(X,Y);
% linehandle=plotpv(net.IW{1},net.b{1});

% 训练感知器神经网络
E=1;
% 迭代次数
net.adaptParam.passes=1;
% 初始化网络
net=init(net);
% 根据网络权值绘制分类器曲线
linehandle=plotpc(net.IW{1},net.b{1});
pause
% 按步执行训练
times=0
while(sse(E))
    times=times+1
%     自适应学习
% 参数：net需要训练的网络，X输入，Y期望输出
% 返回值：net训练后的网络，y网络的输出，E网络误差
    [net,y,E]=adapt(net,X,Y);
    linehandle=plotpc(net.IW{1},net.b{1},linehandle);
    drawnow;
end;
pause

% 测试数据
p=[0.5;1.6];
% 用训练得到的网络计算输出分类
a=net(p);
% 绘制分类结果
plotpv(p,a);
ThePoint=findobj(gca,'type','line');
set(ThePoint,'Color','red');
hold on;
% 绘制样本和训练结果曲线
plotpv(X,Y);
plotpc(net.IW{1},net.b{1});
hold off;
axis([-2,2,-2,2]);
disp('The End')