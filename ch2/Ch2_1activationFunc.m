% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% 绘制各种激活函数曲线
% Author：daizhe
% Date:2020-09-16
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

N=3;

subplot(N,2,1);
% 分段线性函数
x = -10:0.1:10;
y = 0.*(x<-5)+(x/5+1)/2.*(x>-5&x<5)+1.*(x>=5);
plot(x,y,'-r');
hold on;
y2 = 0.*(x<-2)+(x/2+1)/2.*(x>-2&x<2)+1.*(x>=2);
plot(x,y2,'-b');
xlabel('net'),ylabel('y')
legend('a=5','a=2')
title('分段线性函数');

subplot(N,2,2);
plot(x,purelin(x),'LineWidth',2);
title('purelin');

% logistic函数
subplot(N,2,3);
x = -10:0.1:10;
y = 1./(1+exp(-x/1));
plot(x,y,'-r');
hold on;
y2 = 1./(1+exp(-x/0.5));
plot(x,y2,'-b');
xlabel('net'),ylabel('y')
legend('T=1','T=0.5')
title('logistic');

% 双曲正切函数
subplot(N,2,4);
x = -10:0.1:10;
y = (exp(x)-exp(-x))./(exp(x)+exp(-x));
plot(x,y,'-r');
xlabel('net'),ylabel('y')
title('tanh');

% 单位阶跃函数
% hardlim=1(x>=0),0(x<0)
% hardlims=1(x>=0),-1(x<0)
subplot(N,2,5);
n=-5:0.01:5;
plot(n,hardlim(n),'LineWidth',2);
title('hardlim');
subplot(N,2,6);
plot(n,hardlims(n),'LineWidth',2);
title('hardlims')