#!/usr/bin/python
# -*- coding: UTF-8 -*-

# 对TensorFlow自带实例中的手写数据集MNIST进行识别
# 采用了两层CNN网络
# @Time     : 2020/11/11 下午12:19
# @Author   : venice
# @File     : MNISTSoftmax.py
# @Version  : 1.0

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data


def weight_variable(shape):
    """
    返回指定形状的变量，初值以正态分布
    :param shape:
    :return:
    """
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bais_variable(shape):
    """
    返回指定形状的变量，初值都是0.1
    :param shape:
    :return:
    """
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def conv2d(x, W):
    """
    卷积计算
    :param x:
    :param W:
    :return:
    """
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
    """
    最大池化
    :param x:
    :return:
    """
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME")


# 读取数据
mnist = input_data.read_data_sets("MNIST_DATA/", one_hot=True)

# 创建x占位符，代表任意个784维的输入
x = tf.placeholder(tf.float32, [None, 784])
# 期望输出的占位符
y_ = tf.placeholder(tf.float32, [None, 10])

# 将图片从784维向量还原为28*28的矩阵
x_image = tf.reshape(x, [-1, 28, 28, 1])

# 第一层卷积
W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bais_variable([32])
h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

# 第二层卷积
W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bais_variable([64])
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

# 第一全连接层，输出为1024维向量
W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bais_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# 第二全连接层，输出为10，对应10个类别
W_fc2 = weight_variable([1024, 10])
b_fc2 = bais_variable([10])
y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

# 定义误差为交叉熵
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))

train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

# 定义测试的准确率
correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# 创建一个TensorFlow的session会话
sess = tf.InteractiveSession()
# 初始化所有变量并分配内存，变量都是保存在session中的
sess.run(tf.global_variables_initializer())

# 开始迭代
for i in range(1000):
    # 每次取训练集中的50个数据
    batch = mnist.train.next_batch(50)
    # 每100次输出准确率结果
    if i % 100 == 0:
        train_accuracy = accuracy.eval(feed_dict={x: batch[0], y_: batch[1], keep_prob: 1.0})
        print("step %d,training accuracy %g" % (i, train_accuracy))
    train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})

# 输出运行结果
print("test accuracy %g" % accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))
