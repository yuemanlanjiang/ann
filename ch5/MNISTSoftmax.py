#!/usr/bin/python
# -*- coding: UTF-8 -*-

# 对TensorFlow自带实例中的手写数据集MNIST进行识别
# 采用了Softmax回归方法
# @Time     : 2020/11/11 下午12:19
# @Author   : venice
# @File     : MNISTSoftmax.py
# @Version  : 1.0

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

# 读取数据
mnist = input_data.read_data_sets("MNIST_DATA/", one_hot=True)

# 创建x占位符，代表任意个784维的输入
x = tf.placeholder(tf.float32, [None, 784])
# 参数W和b是变量，10代表是10维的输出
W = tf.Variable(tf.zeros([784, 10]))
b = tf.Variable(tf.zeros([10]))
# y是softmax计算的输出
y = tf.nn.softmax(tf.matmul(x, W) + b)
# 期望输出的占位符
y_ = tf.placeholder(tf.float32, [None, 10])

# 定义误差为交叉熵
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y)))
# 梯度下降法对误差进行优化
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)

# 创建一个TensorFlow的session会话
sess = tf.InteractiveSession()
# 初始化所有变量并分配内存，变量都是保存在session中的
tf.global_variables_initializer().run()

# 开始迭代
for _ in range(1000):
    # 每次取训练集中的100个数据，batch_xs是输入，batch_ys是期望输出的表桥
    batch_xs, batch_ys = mnist.train.next_batch(100)
    # 在session中运行梯度下降法
    sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

# 比较得出正确的预测结果
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
# 计算准确率
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
# 输出运行结果
print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
