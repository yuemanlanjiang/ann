#!/usr/bin/python
# -*- coding: UTF-8 -*-

# @Time     : 2020/11/11 上午10:44
# @Author   : venice
# @File     : MNISTDemo.py
# @Version  : 1.0

import os
# 实际安装的是pillow，原来的PIL在Python3.x中已经不可用
from PIL import Image

# tensorflow.examples.tutorials.mnist是TensorFlow的教学实例模块
from tensorflow.examples.tutorials.mnist import input_data

# 导入mnist数据，数据不存在时自动下载，保存到MNIST_DATA目录下
mnist = input_data.read_data_sets("MNIST_DATA/", one_hot=True)



# 训练数据的大小
print(mnist.train.images.shape)
print(mnist.train.labels.shape)

# 验证数据的大小
print(mnist.validation.images.shape)
print(mnist.validation.labels.shape)

# 测试数据的大小
print(mnist.test.images.shape)
print(mnist.test.labels.shape)

# 保存训练样本为图片格式
save_dir = 'MNIST_DATA/raw'
if os.path.exists(save_dir) is False:
    os.makedirs(save_dir)
for i in range(20):
    # 训练数据中的第i张图片，序号从0开始
    image_array = mnist.train.images[i, :]
    # 图片的大小是28*28的，实际保存的是28*28=784维的向量，需要转换一下
    image_array = image_array.reshape(28, 28)
    # 保存的图片名
    filename = save_dir + 'mnist_train_%d.jpg' % i
    # 保存数据为图片
    Image.fromarray((image_array*255).astype('uint8'), mode='L').convert('RGB').save(filename)
    # 下面的方法已经不支持，用上面的方法保存图片
    # scipy.misc.toimage(image_array, cmin=0.0, cmax=1.0).save(filename)
