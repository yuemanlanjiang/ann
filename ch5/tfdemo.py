#!/usr/bin/python
# -*- coding: UTF-8 -*-

# @Time     : 2020/11/11 上午10:35
# @Author   : venice
# @File     : tfdemo.py
# @Version  : 1.0

import tensorflow as tf
import os

hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello))

# os.environ["CUDA_VISIBLE_DEVICES"]="0"
# a=tf.constant(2)
# b=tf.constant(3)
# sess = tf.Session()
# # print(sess.run(hello))
# print("a:%i" % sess.run(a),"b:%i" % sess.run(b))
# print("Addition with constants: %i" % sess.run(a+b))
# print("Multiplication with constant:%i" % sess.run(a*b))