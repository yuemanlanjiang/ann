% 用水量数据预测
clear,clc

a=[0.4366 0.6673 0.6793;
    0.4823 0.5939 0.7112;
    0.5201 0.6387 0.8341;
    0.4872 0.6583 0.8331;
    0.4656 0.5978 0.7786;
    0.4693 0.5387 0.8114;
    0.4572 0.6112 0.8416;
    0.5248 0.6539 0.8639;
    0.4983 0.5972 0.7895];
for i=1:6
    p(i,:)=[a(i,:),a(i+1,:),a(i+2,:)];
end
p

p_train=p(1:5,:)
t_train=a(4:8,:)
p_test=p(6,:)
t_test=a(9,:)

p_train=p_train';
t_train=t_train';
p_test=p_test';

threshold=[0 1;0 1;0 1;0 1;0 1;0 1;0 1;0 1;0 1];

% net=newelm(p_train,t_train,[13,3],{'tansig','purelin'});
net=elmannet(1:3,20);
net.trainparam.epochs=10000;
net.trainparam.show=10;
net=init(net);
net=train(net,p_train,t_train);

y=sim(net,p_test)
error=y'-t_test;
MSE=mse(error)
plot(1:1:3,error,'--ro');
title('Elman神经网络预测误差图');
set(gca,'Xtick',[1:3]);
xlabel('时间点');
ylabel('误差');
