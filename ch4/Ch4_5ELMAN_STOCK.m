clear,clc
close all;

% 读入数据
filename='data/benchmark3.xlsx';
[num,cell,raw]=xlsread(filename);
stockData=num;
% plot(1:300,stockData);
% title('300期股价变化');

% 数据归一化
mi=min(stockData)
ma=max(stockData)
stockData=(stockData-mi)/(ma-mi);
% 训练数据
trainData=stockData(1:150)';
% 测试数据
testData=stockData(151:300)';

% 构造训练数据输入
P=[];
for i=1:150-5
    P=[P;trainData(i:i+4)];
end
P=P';
% 期望输出
T=[trainData(6:150)];

% 创建Elman网络
threshold=[0 1;0 1;0 1;0 1;0 1];
net=newelm(threshold,[0,1],[20,1],{'tansig','purelin'});
% 网络训练
net.trainParam.epochs=1000;
net=init(net);
net=train(net,P,T);
% 仿真及误差
Y=sim(net,P);
error=Y-T;
fprintf('mse=%f\n',mse(error));

% 测试数据进行测试
Pt=[];
for i=1:150-5
    Pt=[Pt;testData(i:i+4)];
end
Pt=Pt';
Yt=sim(net,Pt);

% 归一化还原
T=T*(ma-mi)+mi;
Y=Y*(ma-mi)+mi;
Yt=Yt*(ma-mi)+mi;
testData=testData*(ma-mi)+mi;

subplot(2,1,1);
plot(6:150,T,'b-',6:150,Y,'r-');
title('训练数据');
legend('真实值','仿真结果');
subplot(2,1,2);

plot(6:150,testData(6:150),'b-',6:150,Yt,'r-');
title('测试数据');
legend('真实值','预测结果');