% TSP Solving by Hopfield Neural Network

function TSP_hopfield()
clear,clc
close all;

% 初始化参数
A=1.5;
D=1;
u0=0.02;
step=0.01;

% 城市位置坐标
% citys=[0.1 0.1
% 0.9 0.5
% 0.9 0.1
% 0.45 0.9
% 0.9 0.8
% 0.5 0.5
% 0.1 0.45
% 0.45 0.1];
citys=[0.1 0.6
    0.8 0.3
    0.5 0.6
    0.4 0.9
    0.7 0.1
    0.5 0.5
    0.7 0.3
    0.8 0.8
    0.2 0.9
    0.4 0.1];
N=size(citys,1);
% 计算初始路径长度
Initial_Length=Initial_RouteLength(citys)

% 城市间的距离
DistanceCity=dist(citys,citys')

% step 3
u=2*rand(N,N)-1;
% 输入
U=0.5*u0*log(N-1)+u;
% 输出
V=(1+tanh(U/u0))/2;

%CheckR=[];
%Ep(1)=10;
for k=1:2000
    times(k)=k;
    
%     step 4
    dU=DeltaU(V,DistanceCity,A,D);
    
%     step 5
    U=U+dU*step;
    
%     step 6
    V=(1+tanh(U/u0))/2;
    
%     step 7 计算能量函数
    E=Energy(V,DistanceCity,A,D);
    Ep(k)=E;
%     if(Ep(k)>Ep(k-1))
%         break;
%     end
%     step 8 检查路径合法性
    [V1,CheckR]=RouteCheck(V);


end
% step 9
if (CheckR==0)
    Final_E=Energy(V1,DistanceCity,A,D);
    Final_Length=Final_RouteLength(V1,citys);       % 计算最终路径长度
    disp('迭代次数');k
    disp('寻优路径矩阵：');V1
    disp('最优能量函数：');Final_E
    disp('初始路程：');Initial_Length
    disp('最短路程：');Final_Length
    PlotR(V1,citys);    % 寻优路径作图
else
    disp('寻优路径无效');
end


figure(2);
plot(times,Ep,'r');
title('Energy Function Change');
xlabel('k');
ylabel('E');


% % % % % % 计算最终总路程
function L0=Initial_RouteLength(citys)
citys=[citys;citys(1,:)];
[r,c]=size(citys);
L0=0;
for i=2:r
    L0=L0+dist(citys(i-1,:),citys(i,:)');
end

% % % % % % 计算最终总路程
function L=Final_RouteLength(V,citys)
[xxx,order]=max(V);
New=citys(order,:);
New=[New;New(1,:)];
[rows,cs]=size(New);
L=0;
for i=2:rows
    L=L+dist(New(i-1,:),New(i,:)');
end

% % % % % % 计算du
function du=DeltaU(V,d,A,D)
[n,n]=size(V);
t1=repmat(sum(V,2)-1,1,n);
t2=repmat(sum(V,1)-1,n,1);
PermitV=V(:,2:n);
PermitV=[PermitV V(:,1)];
t3=d*PermitV;
du=-1*(A*t1+A*t2+D*t3);

% % % % % % 计算能量函数
function E=Energy(V,d,A,D)
[n,n]=size(V);
t1=sumsqr(sum(V,2)-1);
t2=sumsqr(sum(V,1)-1);
PermitV=V(:,2:n);
PermitV=[PermitV V(:,1)];
temp=d*PermitV;
t3=sum(sum(V.*temp));
E=0.5*(A*t1+A*t2+D*t3);

% % % % % % % 路径寻优作图
function PlotR(V,citys)
figure;
citys_origin=citys;
citys=[citys;citys(1,:)];
[xxx,order]=max(V);
New=citys(order,:);
New=[New;New(1,:)];

subplot(1,2,1)
plot(citys(1,1),citys(1,2),'*')     % first city
hold on
plot(citys(2,1),citys(2,2),'+')     % second city
hold on
plot(citys(:,1),citys(:,2),'o-')
for i=1:length(citys_origin)
    text(citys_origin(i,1),citys_origin(i,2),['   ' num2str(i)])
end
xlabel('X axis')
ylabel('Y axis')
title('Original Route')
axis([0 1 0 1])
grid on

subplot(1,2,2)
plot(New(1,1),New(1,2),'*')       % first city
hold on
plot(New(2,1),New(2,2),'+')       % second city
hold on
plot(New(:,1),New(:,2),'o-')
for i=1:length(citys_origin)
    text(citys_origin(i,1),citys_origin(i,2),['  ' num2str(i)])
end
xlabel('X axis')
ylabel('Y axis')
title('TSP solution')
axis([0 1 0 1])
axis on
grid on


% % % % % % 标准化路径，并检查路径合法性，要求每行每列只有一个“1”
function [V1,CheckR]=RouteCheck(V)
[rows,cols]=size(V);
V1=zeros(rows,cols);
[XC,Order]=max(V);
for j=1:cols
    V1(Order(j),j)=1;
end
C=sum(V1);
R=sum(V1');
CheckR=sumsqr(C-R);

%web -browser http://www.ilovematlab.cn/thread-44738-1-1.html
