% 离散Hopfield进行点阵图像还原
clear,clc

% 读取图片，处理成灰度图片，处理为1或-1
img1=imread('one.jpg');
if length(size(img1))==3
    one=rgb2gray(img1)
else
    ont=img1;
end
one=trans(one);
[m,n]=size(one);

% 读取图片
img8=imread('eight.jpg');
if length(size(img8))==3
    eight=rgb2gray(img8);
else
    eight=img8;
end
eight=trans(eight);

% 训练样本
T=[one eight];
% 构建DHNN
net=newhop(T);

% 带噪声的图片
noisy_img1=one;
noisy_img8=eight;
for i=1:m*n
    a=rand;
    if a<0.5
        noisy_img1(i)=-one(i);
        noisy_img8(i)=-eight(i);
    end
end

% 噪声图片送入网络进行仿真
noisy_one={(noisy_img1)'};
repair_one=sim(net,{m,n},{},noisy_one');
A=repair_one{10}'
A=untrans(A)

noisy_eight={(noisy_img8)'};
repair_eight=sim(net,{m,n},{},noisy_eight');
B=repair_eight{10}'
B=untrans(B)

% 绘图
subplot(3,2,1);
imshow(one);
title('点阵图片1');
subplot(3,2,3);
imshow(noisy_img1);
title('随机干扰图片1');
subplot(3,2,5);
imshow(A);
title('恢复图片1');

subplot(3,2,2);
imshow(eight);
title('点阵图片8');
subplot(3,2,4);
imshow(noisy_img8);
title('随机干扰图片8');
subplot(3,2,6);
imshow(B);
title('恢复图片8');

% 图片处理未1和-1
function X=trans(A)
    [r,c] = size(A);    % 读取行r、列c
    X=ones(r,c);
    for i = 1:r        % 建立for循环嵌套
        for k = 1:c
            if A(i,k)>128     % 读取矩阵每个位置数据，先行后列
                X(i,k)=1;
            else
                X(i,k)=-1;
            end
        end
    end
end

% 图片反处理
function X=untrans(A)
    [r,c] = size(A);    % 读取行r、列c
    X=ones(r,c);
    for i = 1:r        % 建立for循环嵌套
        for k = 1:c
            if A(i,k)>-1     % 读取矩阵每个位置数据，先行后列
                X(i,k)=1;
            else
                X(i,k)=-1
            end
        end
    end
end