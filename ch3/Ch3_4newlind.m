% 使用newlind函数，直接设计一个线性神经网络，不需要再学习直接用于仿真

%% 生成数据
x=-5:5;                             % 输入
y=3*x-7;                            % 直线方程为 
randn('state',2);                   % 设置种子，便于重复执行
y=y+randn(1,length(y))*1.5;			% 加入噪声的直线
plot(x,y,'o');

%%设计网络
P=x;T=y;
net=newlind(P,T);                   % 用newlind建立线性层

%%仿真
new_x=-5:.2:5;                      % 新的输入样本
new_y=sim(net,new_x);				% 仿真
hold on;
plot(new_x,new_y);
legend('原始数据点','最小二乘拟合直线');
net.iw                              % 权值

net.b                               % 偏置

title('newlind用于最小二乘拟合直线');
