% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% BP网络工具箱函数：线性拟合，feedforwardnet和cascadeforwardnet函数
% Author：daizhe
% Date:2020-10-05
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear cls;
% % 训练数据
% P=[0 1 2 3 4 5 6 7 8 9 10];
% T=[0 1 2 3 4 3 2 1 2 3 4];
% 第二组训练数据
% P=-4:.5:4;
% T=P.^2-P;
% 第三组训练数据，自带数据集
[P,T]=simplefit_dataset;

% 创建一个BP网络
net1=feedforwardnet(10);
net1.trainParam.epochs=50;
net1=train(net1,P,T);
perf=perform(net1,P,T);
Y1=sim(net1,P)

% 创建一个级联的前向网络
net2=cascadeforwardnet(10);
net2.trainParam.epochs=50;
net2=train(net2,P,T);
perf=perform(net1,P,T);
Y2=sim(net2,P)

% 绘图
plot(P,T,'ro-');
hold on;
plot(P,Y1,'^m-');
plot(P,Y2,'*-k');
title('feedforwardnet & cascadeforwardnet')
legend('原始数据','feedforwardnet拟合','cascadeforwardnet拟合')