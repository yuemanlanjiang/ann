% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% BP网络工具箱函数：训练函数
% Author：daizhe
% Date:2020-10-05
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear cls;
% 训练数据
P=[0 1 2 3 4 5 6 7 8 9 10];
T=[0 1 2 3 4 3 2 1 2 3 4];
% 第二组训练数据
% P=-4:.5:4;
% T=P.^2-P;
% 第三组训练数据，自带数据集
% [P,T]=simplefit_dataset;

% traingd
net1=feedforwardnet(10,'traingd');
net1.trainParam.epochs=50;
net1=train(net1,P,T);
perf=perform(net1,P,T);
Y1=sim(net1,P)

net2=feedforwardnet(10,'traingdm');
net2.trainParam.epochs=50;
net2=train(net2,P,T);
perf=perform(net2,P,T);
Y2=sim(net2,P)

net3=feedforwardnet(10,'trainbfg');
net3.trainParam.epochs=50;
net3=train(net3,P,T);
perf=perform(net3,P,T);
Y3=sim(net3,P)


% 绘图
plot(P,T,'ro-');
hold on;
plot(P,Y1,'^m-');
plot(P,Y2,'*-k');
plot(P,Y3,'*-g');
title('BP网络训练函数')
legend('原始数据','traingd','traingdm','trainbfg')