% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% Adaline网络应用：自适应滤波器
% Author：daizhe
% Date:2020-10-05
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear,clc

% 时间变量
time=0.5:0.005:50;
% 初始EEG信号
eeg_0=(rand(1,9901)-0.5)*4;
% 噪声源
noise=sin(time);
% 采集的被污染的EEG信号
eeg=eeg_0+noise;
% 噪声源作为输入
input=con2seq(noise);
% 被污染的EEG信号作为期望输出
target=con2seq(eeg);
% 构造Adaline网络
net=newlin(minmax(eeg),1,[1 2],0.0005);
% 网络训练
[net,output,e]=adapt(net,input,target);

% 绘制图形
subplot(4,1,1);
plot(time,eeg,'r');
ylabel('混合信号');
subplot(4,1,2);
output=seq2con(output);
plot(time,output{1},'k-');
ylabel('网络输出信号');
subplot(4,1,3);
e=seq2con(e);
plot(time,e{1},'k-');
ylabel('恢复的EEG信号');
subplot(4,1,4);
e=seq2con(e);
plot(time,eeg_0,'k-');
ylim([-5 5]);
ylabel('原始EEG信号');
hold on;
plot(time,noise,'r-');