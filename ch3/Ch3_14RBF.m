% 径向基网络newrf和newrbe函数
tic
P=-2:.5:2;
rand('state',pi);
T=sin(2*P)+rand(1,length(P));	% 在二次函数中加入噪声
test=-2:.2:2;

% 建立严格的径向基函数网络
net1=newrbe(P,T,5);			
out1=sim(net1,test);			% 仿真测试
% 建立径向基函数网络
net2=newrb(P,T,1);	
out2=sim(net2,test);			% 仿真测试

plot(P,T,'o');
hold on;
plot(test,out1,'b-');
hold on;
plot(test,out2,'r--');
legend('输入的数据','newrbe','newrb');
