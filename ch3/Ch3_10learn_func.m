% BP学习函数，learngd和learngdm

clear,clc

% 随机数据
gW=rand(5,4);
% 学习速率
lp.lr=0.05;
% 学习状态
ls=[];
% 计算变化率
[dW,ls]=learngd([],[],[],[],[],[],[],gW,[],[],lp,ls)

% 动量常数
lp.mc=0.8;
% 计算变化率
[dW,ls]=learngdm([],[],[],[],[],[],[],gW,[],[],lp,ls)