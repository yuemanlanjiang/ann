% % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% Adaline网络应用：线性拟合，newlin函数
% Author：daizhe
% Date:2020-10-05
% Version：1.0
% % % % % % % % % % % % % % % % % % % % % % % % % % % % 

clear,clc

% 样本点
x=[0.21 0.56 1.01 0.72 0.54 1.03];
y=[0.29 0.57 1.30 0.93 0.74 1.25];

% newlin方法构造adaline网络
% 参数分别是：输入向量的典型值，输出个数，输入延迟向量，学习率
net=newlin(x,y,0,0.01);
% 初始化网络
net=init(net);
% 迭代轮次
net.trainParam.epochs=2000;
% 训练目标最小误差
net.trainParam.goal=0;
% 开始网络训练
net=train(net,x,y);

% 根据训练网络仿真数据
simy=sim(net,x);

% 绘制样本点及拟合曲线
figure;
plot(x,y,'o');
hold on;
plot(x,simy,'b');